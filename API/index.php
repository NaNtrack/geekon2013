<?php
//Step 1: Require the Slim Framework
require 'Slim/Slim.php';
require 'App/geoclues.php';
\Slim\Slim::registerAutoloader();
include ('Slim/Core/database.php');
//Created conexion
$db = new database(); //i created a new object

//Step 2: Instantiate a Slim application
$app = new \Slim\Slim();
// Step 3: ROUTES
$app->get('/cluesList/:user_id', function ($user_id) use ($db){
    $cluesList = $db->query('
        SELECT p.id AS PlaceId,
            p.orden PlaceOrden,
            p.name PlaceName,
            c.id ClueId,
            c.text ClueText,
            c.lock ClueLock,
            c.type ClueType,
            c.textlock ClueTextlock
        FROM places p
            LEFT JOIN clues c ON p.id = c.place_id');
    foreach ($cluesList as $key => $value) {
        $result[$value['PlaceId']]['name'] = $value['PlaceName'];
        $result[$value['PlaceId']]['orden'] = $value['PlaceOrden'];
        $result[$value['PlaceId']]['id'] = $value['PlaceId'];
        $result[$value['PlaceId']]['clues'][$value['ClueId']]['id'] = $value['ClueId'];
        $result[$value['PlaceId']]['clues'][$value['ClueId']]['text'] = $value['ClueText'];
        $result[$value['PlaceId']]['clues'][$value['ClueId']]['type'] = $value['ClueType'];
        $result[$value['PlaceId']]['clues'][$value['ClueId']]['lock'] = $value['ClueLock'];
        $result[$value['PlaceId']]['clues'][$value['ClueId']]['textlock'] = $value['ClueTextlock'];
        $result[$value['PlaceId']]['clues'][$value['ClueId']]['placeId'] = $value['PlaceId'];
    }
    echo json_encode($result);
});

$app->get('/geoClues(/:user_id)', 'geoClues');

//Step 4: Run the Slim application
$app->run();