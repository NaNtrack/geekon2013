<?php

function geoClues($user_id='') {

    $app = \Slim\Slim::getInstance();

    if (is_null($user_id)){
		$result = array();
    } else {

		$result = array(
			'map' => array('zoom' => 16),
			'center' => array('lat' => 0, 'lng' => 0),
			'clues' => array(
				0 => array(
					'center' => array('lat' => -33.417974, 'lng' => -70.601858),
					'stroke' => "#FF0000",
					'fill' => "#FF0000",
					'strokeOp' => 0.8,
					'strokeWeight' => 2,
					'fillOp' => 0.35,
					'radius' => 45
					),
				1 => array(
					'center' => array('lat' => -33.414992, 'lng' => -70.603886),
					'stroke' => "#639924",
					'fill' => "#c5dc6c",
					'strokeOp' => 0.8,
					'strokeWeight' => 2,
					'fillOp' => 0.35,
					'radius' => 55
					),
				2 => array(
					'center' => array('lat' => -33.418852, 'lng' => -70.607255),
					'stroke' => "#FF0000",
					'fill' => "#FF0000",
					'strokeOp' => 0.8,
					'strokeWeight' => 2,
					'fillOp' => 0.35,
					'radius' => 65
					)
			)
		);

		$result['center']['lat'] = ($result['clues'][0]['center']['lat'] + $result['clues'][1]['center']['lat'] + $result['clues'][2]['center']['lat'])/3;
		$result['center']['lng'] = ($result['clues'][0]['center']['lng'] + $result['clues'][1]['center']['lng'] + $result['clues'][2]['center']['lng'])/3;

    }

    echo json_encode($result);

}