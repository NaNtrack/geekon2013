# ************************************************************
# Sequel Pro SQL dump
# Versión 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.19)
# Base de datos: geekon2013
# Tiempo de Generación: 2013-07-25 16:26:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla clues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clues`;

CREATE TABLE `clues` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `place_id` int(11) NOT NULL,
  `text` varchar(500) DEFAULT NULL,
  `lock` tinyint(1) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `textlock` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `places_id` (`place_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clues` WRITE;
/*!40000 ALTER TABLE `clues` DISABLE KEYS */;

INSERT INTO `clues` (`id`, `place_id`, `text`, `lock`, `type`, `textlock`)
VALUES
	(1,1,'texto pista 1',0,'GLOBAL',NULL),
	(2,1,'texto pista 2',1,'SOCIAL','Tener 5 menciones en twitter'),
	(3,1,'texto pista 3',1,'SOCIAL','Tener 10 menciones en twitter'),
	(4,2,'texto pista 1',0,'GLOBAL',NULL),
	(5,2,'texto pista 2',1,'SOCIAL','Tener 5 menciones en twitter'),
	(6,2,'texto pista 3',1,'SOCIAL','Tener 10 menciones en twitter'),
	(7,3,'texto pista 1',0,'GLOBAL',NULL),
	(8,3,'texto pista 2',1,'SOCIAL','Tener 5 menciones en twitter'),
	(9,3,'texto pista 3',1,'SOCIAL','Tener 10 menciones en twitter');

/*!40000 ALTER TABLE `clues` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla places
# ------------------------------------------------------------

DROP TABLE IF EXISTS `places`;

CREATE TABLE `places` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `orden` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;

INSERT INTO `places` (`id`, `orden`, `name`)
VALUES
	(1,1,'Lugar 1'),
	(2,2,'Lugar 2'),
	(3,3,'Lugar 3');

/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Volcado de tabla users_clues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_clues`;

CREATE TABLE `users_clues` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `clue_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
