<?php

/**
 * @author estebanzeus
 * @copyright 2013
 */
require 'config.php';
//create a class for make connection
class database {
    var $host       = HOST;
    var $username   = USERNAME;    // specify the sever details for mysql
    Var $password   = PASS;
    var $database   = DATABASE;
    var $myconn;

    // create a function for connect database
    function connectToDatabase() {
        /* Connect to an ODBC database using driver invocation */
        $dsn = 'mysql:dbname='.$this->database.';host='.$this->host;
        $user = $this->username;
        $password = $this->password;

        try {
            $dbh = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
        return $dbh;
    }

    // selecting the database.
    function selectDatabase(){
    	//use php inbuild functions for select database
        mysql_select_db($this->database);
        // if error occured display the error message
        if(mysql_error()){
            echo "Cannot find the database ".$this->database;
        }
        echo "Database selected..";
    }
    // close the connection
    function closeConnection(){
        mysql_close($this->myconn);
        echo "Connection closed";
    }

    function query($query, $debug = false){
        $conn = $this->connectToDatabase(); // connected to the database
        $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
        foreach ($conn->query($query) as $row) {
            $result[] = $row;
        }
        if($debug){
            echo'<pre>';
            echo $query.'<br>';
            print_r($result);
            echo'</pre>';
            die;
        }
        return($result);
    }
}
?>