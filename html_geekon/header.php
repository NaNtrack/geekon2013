<header>
	<nav class="best_nav">
	  <a href="index.php" <?php if (strpos($_SERVER['REQUEST_URI'], 'index.php')) : ?> class="btn_home active"<?php endif; ?>><i class="icon-home"></i></a>
	  <a href="pistas.php"<?php if (strpos($_SERVER['REQUEST_URI'], 'pistas.php')) : ?> class="active"<?php endif; ?>><i class="icon-eye-close"></i> <small>Pistas</small></a>
	  <a href="mapa.php"<?php if (strpos($_SERVER['REQUEST_URI'], 'mapa.php')) : ?> class="active"<?php endif; ?>><i class="icon-map-marker"></i><small> Mapa</small></a>
	  <a href="redes_sociales.php"<?php if (strpos($_SERVER['REQUEST_URI'], 'redes_sociales.php')) : ?> class="active"<?php endif; ?>><i class="icon-comments"></i> <small>Redes sociales</small></a>
	  <a href="perfil.php" <?php if (strpos($_SERVER['REQUEST_URI'], 'perfil.php')) : ?> class="active"<?php endif; ?>><i class="icon-user"></i> <small>Mi perfil</small></a>
	</nav>
  </header>