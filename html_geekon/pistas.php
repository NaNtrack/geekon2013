<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Sign in &middot; Twitter Bootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/run_and_win.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>
    <?php require_once 'header.php'; ?>
    <div class="container row-fluid show-grid">
      <div class="span12 check_container_all">
        <div class="alert alert-success">
          <a type="button" class="close" data-dismiss="alert">&times;</a>
          <h4>Bienvenido a nuestra aplicación</h4>
          Te queremos entregar más información , <a href="#">Ver detalles</a>
        </div>
        <div id="places" class="row-fluid show-grid">
          <a id="tplPlaces" href="#" class="" style="display:none">
            <span>
              <i class="icon-2x"></i>
            </span>
          </a>
        </div>
        <div class="row-fluid show-grid content_pistas" style="display:none" id="tplContentClues">
            <div class="input-append cont_checkin">
              <input class="input_checkin" type="text" placeholder="Ingresa tu código">
              <button class="btn btn_checkin btn-info" type="button">Check in</button>
          </div>

          <div class="item_pista" id="tplClues" style="display:none">
            <div class="padding_random">
              <div class="icon">
                <i class="icon-2x"></i>
              </div>
              <div class="info_pista"></div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/run_and_win.js" type="text/javascript"></script>
  </body>
</html>
