<?php
header('Content-type: application/json');
$action = @$_GET['a'];

$response = array();
switch ($action) {
	case 'tweet':
		require_once dirname(dirname(__FILE__)).'/Classes/Twitter.php';
		$tweet_id = Twitter::postMessage($_POST['message'], $_POST['lat'], $_POST['lng']);
		if ($tweet_id) {
			$response = array(
				'status' => 'OK',
				'tweet' => $tweet_id
			);
		} else {
			$response = array(
				'status' => 'ERROR',
				'error' => 'Unable to tweet your message'
			);
		}
		break;
	case 'localization':
		require_once dirname(dirname(__FILE__)).'/Classes/Twitter.php';
		$location = Twitter::reverseGeoCode($_POST['lat'], $_POST['lng']);
		if ($location) {
			$response = array(
				'status' => 'OK',
				'location' => $location
			);
		} else {
			$response = array(
				'status' => 'ERROR',
				'error' => 'Unable to get the location'
			);
		}
		break;
	default:
		$response = array(
			'status' => 'ERROR',
			'error' => 'Invalid request'
		);
		break;
}
echo json_encode($response);
