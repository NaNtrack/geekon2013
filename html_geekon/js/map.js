$(document).ready(function(){

  var endpoint = $(document).find("meta[name=endpoint]").attr('content');
  var map_elm_id = $(document).find("[data-map=true]").attr('id');

  var map_data = {};
  var mapOptions, map, rad;
  var circles = [];

  $.getJSON( '../API/geoClues/1234', {} )
  .done(function( data ) {

    map_data = data;

    // Enable the visual refresh
    google.maps.visualRefresh = true;

    mapOptions = {
      zoom: map_data.map.zoom,
      center: new google.maps.LatLng(map_data.center.lat, map_data.center.lng),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById(map_elm_id), mapOptions);

    for (var point in map_data.clues) {

      var circleOptions = {
        strokeColor: map_data.clues[point].stroke,
        strokeOpacity: map_data.clues[point].strokeOp,
        strokeWeight: map_data.clues[point].strokeWeight,
        fillOpacity: map_data.clues[point].fillOp,
        fillColor: map_data.clues[point].fill,
        center: new google.maps.LatLng(map_data.clues[point].center.lat,map_data.clues[point].center.lng),
        radius: map_data.clues[point].radius,
        map: map
      };

      c = new google.maps.Circle(circleOptions);

      circles.push(c);

    }

  })
  .fail(function( jqxhr, textStatus, error ) {
    var err = textStatus + ', ' + error;
    console.log( "Request Failed: " + err);
  });

  setInterval(function() {

    for (var num in circles) {
      rad = circles[num].getRadius();
      circles[num].setRadius(rad-1);

    }


  }, 5000);

});
