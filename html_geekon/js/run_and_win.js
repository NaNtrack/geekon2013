$(document).ready(function(){
	$('.close').on('click',function(){
		$(this).parent().fadeOut();
	});

	$('.check_container_all').on('click', '.box_check_point', function(event) {
		event.preventDefault();
		var var_index = $(this).data('ref');
		$('.content_pistas').hide();
		$('.check_container_all .box_check_point').removeClass('active');
		if( $(this).hasClass('active')){

		} else{
			$('#pistas_check_'+var_index+'').fadeIn();
			$(this).addClass('active');
		}
		return false;
	});

	$('#message').bind('keyup', function(){
		if ($('#message').val() === '') {
			$('#btnTweet').addClass('disabled');
		} else {
			$('#btnTweet').removeClass('disabled');
		}
	});

	fillClues();
});

function tweet () {
	if ($('#btnTweet').hasClass('disabled')) {
		return false;
	}

	$.ajax({
		url : '/ajax.php?a=tweet',
		type: 'POST',
		data: 'message=' + $('#message').val()+'&lat='+$('#lat').val()+'&lng='+$('#lng').val(),
		async: false,
		cache: false,
		success: function(response){
			if(response.status === 'OK') {
				$('#message').val('');
				$('#btnTweet').addClass('disabled');
			} else {
				alert(response.error);
			}
		},
		error: function(jqXHR, status, errorThrown) {
			alert('Error: ' + status);
		}
	});
	return false;
}

function getLocation () {
	if ($('#btnLocation').hasClass('btn-primary')) {
		$('#btnLocation').removeClass('btn-primary');
		$('#localization').text('');
	} else {
		$('#btnLocation i').addClass('icon-spinner icon-spin');
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(localization_success, localization_error);
		} else {
			localization_error('Geo-localization not supported');
		}
	}
	return false;
}

function localization_success (position) {
	$('#lat').val(position.coords.latitude);
	$('#lng').val(position.coords.longitude);
	$.ajax({
		url : '/ajax.php?a=localization',
		type: 'POST',
		data: 'lat=' + position.coords.latitude+'&lng='+position.coords.longitude,
		async: true,
		cache: false,
		success: function(response){
			$('#btnLocation i').removeClass('icon-spinner icon-spin');
			if(response.status === 'OK') {
				$('#btnLocation').addClass('btn-primary');
				$('#localization').text(response.location);
			} else {
				alert(response.error);
			}
		},
		error: function(jqXHR, status, errorThrown) {
			$('#btnLocation i').removeClass('icon-spinner icon-spin');
			alert('Error: ' + status);
		}
	});
}

function localization_error(msg) {
	$('#localization').text(msg);
	$('#btnLocation i').removeClass('icon-spinner icon-spin');
}

function fillClues () {
	$.getJSON('../API/cluesList/1', function(json, textStatus) {
		$.each(json, function(index, places) {
			var tpl = $('#tplPlaces').clone();
			tpl.removeAttr('id');
			tpl.find('span i').after(places.name);
			if (places.orden === "1") {
				tpl.addClass('span4 box_check_point unlocked');
				tpl.find('span i').addClass('icon-star');
			}else{
				tpl.addClass('span4 box_check_point');
				tpl.find('span i').addClass('icon-pushpin');
			}
			tpl.attr('data-ref', places.orden);
			tpl.show();
			$('#places').append(tpl);
			var tplContentClues = $('#tplContentClues').clone();
			var idTplClues = 'pistas_check_'+places.id;
			tplContentClues.attr('id',idTplClues);
			$('#tplContentClues').after(tplContentClues);
			$.each(places.clues, function(index, clues) {
				var tplClues = $('#tplClues').clone();
				if (clues.lock === "1") {
					tplClues.find('.info_pista').html(clues.textlock);
					tplClues.find('i').addClass('icon-lock');
				}else{
					tplClues.find('.info_pista').html(clues.text);
					tplClues.find('i').addClass('icon-unlock');
					tplClues.addClass('enabled');
				}
				tplClues.show();
				$('#pistas_check_'+clues.placeId).append(tplClues);
			});
		});
		$('#tplPlaces').remove();
	});
}