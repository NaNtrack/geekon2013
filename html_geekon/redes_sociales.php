<?php
header('Content-Type: text/html');
require_once dirname(dirname(__FILE__)) . '/Classes/Twitter.php';
$twitter = new Twitter();
$credentials = Twitter::getCredentials();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Social Networks</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/run_and_win.css" rel="stylesheet">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="../assets/js/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
</head>
<body>
	<?php require_once 'header.php'; ?>
	<div class="container row-fluid show-grid">
		<div class="span12 check_container_all">
			<?php if ($credentials === false) : ?>
				<div class="box_link_twitter">
					<?php $authorize_url = $twitter->getAuthorizeURL('http://dev.geekon.com/twitter_callback.php'); ?>
					<p class="lead">Es necesario que ingreses con tu cuenta de twitter, para dusfrutar al máximo la aplicación</p>
					<p><a class="btn btn-large btn-success btn_to_twitter" href="<?php echo $authorize_url; ?>"><i class="icon-twitter"></i> Twitter</a></p>
				</div>
				<?php if (isset($_GET['error'])) : ?>
				<div class="alert alert-error">
					<a type="button" class="close" data-dismiss="alert">&times;</a>
					<h4>Ups imposible conectarse a twitter</h4>
					Intentalo nuevamente porfis!
				</div>
				<?php endif; ?>
			<?php else : ?>
				<div class="box_twittear">
					<div class="padding_random">
						<textarea id="message" rows="3" placeholder=""></textarea>
						<div class="twitter_actions">
							<a href="#" class="left btn" id="btnLocation" onclick="return getLocation();"><i class="icon-map-marker"></i></a>
							<a href="#" class="right btn btn-info disabled" onclick="return tweet();" id="btnTweet"><i class="icon-twitter"></i> Twittear</a>
						</div>
						<input type="hidden" name="lat" id="lat" value="" />
						<input type="hidden" name="lng" id="lng" value="" />
						<div id="localization"></div>
					</div>
					<?php $tweets_count = Twitter::getTweetsCount('#geekon'); ?>
					<div class="padding_random">
						<div class="box_history">
							<h2>Interacciones recientes</h2>
							<div class="history_scroll">
								<ul id="mitweets">
									<?php foreach ($tweets_count['mytweets'] as $tweet_id => $tweet) : ?>
									<li id="<?php echo $tweet_id ?>"><a href="https://twitter.com/<?php echo $tweet['author']; ?>" target="_blank">@<?php echo $tweet['author']; ?></a>: <?php echo $tweet['text']; ?></li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>

			<?php endif; ?>
		</div>
	</div> <!-- /container -->

	<!-- Le javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/run_and_win.js" type="text/javascript"></script>
</body>
</html>
