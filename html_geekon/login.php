<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Sign in &middot; Twitter Bootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/run_and_win.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container row-fluid show-grid">
      <div class="span12">
        <form class="form-signin" action="redes_sociales.php">
          <h2 class="form-signin-heading">Ingresar</h2>
          <select name="select_country" id="cuntry_id" class="span12">
            <option val="">--Seleccione--</option>
            <option value="CL">Chile</option>
            <option value="PE">Peru</option>
            <option value="MX">Mexico</option>
            <option value="CO">Colombia</option>
          </select>
          <input type="text" class="input-block-level" placeholder="Email">
          <input type="password" class="input-block-level" placeholder="Contraseña">
          <button class="btn btn-large btn-success" type="submit">Ingresar</button>
        </form>
      </div>
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/run_and_win.js" type="text/javascript"></script>
  </body>
</html>
