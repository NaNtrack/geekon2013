<?php

/**
 * Wrapper that handle several twitter interactions
 *
 * @author Julio Araya <jaraycerda@groupon.com>
 */

require_once dirname(dirname(__FILE__)) . '/Libs/tmhOAuth/tmhOAuth.php';

final class Twitter {

	/**
	 * Returns the number of tweets of a user related to a specific hashtag
	 *
	 * @param string $hashTag
	 * @return array An aray with the following metrics
	 * <ul>
	 *	<li><b>mentions</b>: number of different users that mentioned you with the @username and #hashtag</li>
	 *	<li><b>tweets</b>: number of tweets that contains the @username and #hashtag</li>
	 *	<li><b>retweets</b>: number of retweets with that contains the @username and #hashtag</li>
	 *	<li><b>favorites</b>: number of tweets that has been marked as favorites</li>
	 *	<li><b>max_follower_count</b>: The maximum number of followers that someone that mention you have right now</li>
	 * </ul>
	 */
	public static function getTweetsCount($hashTag) {
		
		$formatted_hashTag  = strtolower(trim(str_replace('#', '', $hashTag)));

		$credentials = self::getCredentials();
		if ($credentials === false) {
			return null;
		}

		$mentions = 0;
		$tweets = 0;
		$retweets = 0;
		$favorites = 0;
		$max_followers_count = 0;
		$users_mentions = array();
		$mytweets = array();

		$tmhOAuth = new tmhOAuth($credentials);

		//mentions
		$responseStatus = $tmhOAuth->request('GET', 'https://api.twitter.com/1.1/statuses/mentions_timeline.json', array(
			'count' => 200,
			'include_entities' => 1,
		));
		if ($responseStatus == 200) {
			$response = json_decode($tmhOAuth->response['response'], true);
			foreach ($response as $tweet) {
				$hashTags = $tweet['entities']['hashtags'];
				if (count($hashTags) == 0) {
					continue;
				} else {
					foreach ($hashTags as $hashTag) {
						if (strtolower($hashTag['text']) == $formatted_hashTag) {
							$tweets++;
							$users_mentions[] = $tweet['user']['id'];
							if ($max_followers_count < $tweet['user']['followers_count']) {
								$max_followers_count = $tweet['user']['followers_count'];
							}
							$lower = strtolower($tweet['text']);
							$text = str_replace('#'.$formatted_hashTag, '<a href="https://twitter.com/search?q=%23'.$formatted_hashTag.'&src=hash" target="_blank">#'.$formatted_hashTag.'</a>', $lower);
							$mytweets[$tweet['id']] = array(
								'author' => $tweet['user']['screen_name'],
								'text' => $text
							);
							break;
						}
					}
				}
			}
		}

		usleep(500);

		//user tweets
		$responseStatus = $tmhOAuth->request('GET', 'https://api.twitter.com/1.1/statuses/user_timeline.json', array(
			'user_id' => $credentials['user_id'],
			'count' => 200
		));
		if ($responseStatus == 200) {
			$response = json_decode($tmhOAuth->response['response'], true);
			foreach ($response as $tweet) {
				$hashTags = $tweet['entities']['hashtags'];
				if (count($hashTags) == 0) {
					continue;
				} else {
					foreach ($hashTags as $hashTag) {
						if (strtolower($hashTag['text']) == $formatted_hashTag) {
							$retweets += (int)$tweet['retweet_count'];
							$favorites += (int)$tweet['favorite_count'];
							$lower = strtolower($tweet['text']);
							$text = str_replace('#'.$formatted_hashTag, '<a href="https://twitter.com/search?q=%23'.$formatted_hashTag.'&src=hash" target="_blank">#'.$formatted_hashTag.'</a>', $lower);
							$mytweets[$tweet['id']] = array(
								'author' => $tweet['user']['screen_name'],
								'text' => $text
							);
							break;
						}
					}
				}
			}
		}

		ksort($mytweets);
		
		return array(
			'mentions' => count(array_unique($users_mentions)),
			'tweets' => $tweets,
			'retweets' => $retweets,
			'favorites' => $favorites,
			'max_followers_count' => $max_followers_count,
			'mytweets' => $mytweets
		);
	}

	/**
	 * Return the authorization URL
	 *
	 * @param string $callback The URL callback
	 * @return string The URL on success, null otherwise
	 */
	public function getAuthorizeURL ($callback) {
		$tmhOAuth = new tmhOAuth();
		$responseStatus = $tmhOAuth->request('POST', 'https://api.twitter.com/oauth/request_token', array('oauth_callback' => $callback));
		if ($responseStatus == 200) {
			$response = $tmhOAuth->response;
			$url_parts = $tmhOAuth->extract_params($response['response']);
			if (array_key_exists('oauth_token', $url_parts) && array_key_exists('oauth_token_secret', $url_parts)) {
				return 'https://api.twitter.com/oauth/authorize?oauth_token='.$url_parts['oauth_token'];
			}
		}
		return null;
	}

	/**
	 * Perfirn the final step of the 3-legged authentication and retrieve
	 * the user access token and secret
	 *
	 * @param string $oauth_token The callback oauth token (step 2)
	 * @param string $oauth_verifier
	 * @return boolean True on success, false otherwise
	 */
	public static function getAccessToken ($oauth_token, $oauth_verifier) {
		$tmhOAuth = new tmhOAuth();
		$responseStatus = $tmhOAuth->request('POST', 'https://api.twitter.com/oauth/access_token', array(
			'oauth_token' => $oauth_token,
			'oauth_verifier' => $oauth_verifier
		));
		if ($responseStatus == 200) {
			$response = $tmhOAuth->response;
			$url_parts = $tmhOAuth->extract_params($response['response']);
			if (array_key_exists('oauth_token', $url_parts) && array_key_exists('oauth_token_secret', $url_parts)) {
				self::setCredentials(
					$url_parts['oauth_token'],
					$url_parts['oauth_token_secret'],
					$url_parts['user_id'],
					$url_parts['screen_name']
				);
				return true;
			}
		}
		return false;
	}

	/**
	 * Get the current logged user's credentials
	 *
	 * @return array
	 */
	public static function getCredentials () {
		if(isset($_COOKIE['credentials'])) {
			return json_decode(base64_decode($_COOKIE['credentials']), true);
		}
		return false;
	}

	/**
	 * Serialize the twitter credentials of the current logged user
	 *
	 * @param string $oauth_token
	 * @param string $oauth_token_secret
	 * @param string $user_id
	 * @param string $screen_name
	 */
	private static function setCredentials ($oauth_token, $oauth_token_secret, $user_id, $screen_name) {
		$value = base64_encode(json_encode(array(
			'user_token' => $oauth_token,
			'user_secret' => $oauth_token_secret,
			'user_id' => $user_id,
			'screen_name' => $screen_name
		)));
		setcookie('credentials', $value, time()+86400, '/', $_SERVER['HTTP_HOST']);
	}

	/**
	 * Post a tweet on the user streamline
	 */
	public static function postMessage ($message, $lat, $long) {
		$formatted_message = substr($message, 0, 140);
		if (strpos($formatted_message, '#geekon') === FALSE) {
			$formatted_message .= ' #geekon';
		}

		$credentials = self::getCredentials();
		if ($credentials !== false) {
			$tmhOAuth = new tmhOAuth($credentials);
			$responseStatus = $tmhOAuth->request('POST', 'https://api.twitter.com/1.1/statuses/update.json', array(
				'status' => $formatted_message,
				'lat' => $lat,
				'long' => $long
			));
			if ($responseStatus == 200) {
				$response = json_decode($tmhOAuth->response['response'], true);
				return $response;
			}
		}
		return false;
	}

	/**
	 *
	 * @param float $lat
	 * @param float $lng
	 */
	public static function reverseGeoCode ($lat, $lng) {
		$credentials = self::getCredentials();
		if ($credentials !== false) {
			$tmhOAuth = new tmhOAuth($credentials);
			$responseStatus = $tmhOAuth->request('GET', 'https://api.twitter.com/1.1/geo/reverse_geocode.json', array(
				'lat' => $lat,
				'long' => $lng,
				'max_results' => 1
			));
			if ($responseStatus == 200) {
				$response = json_decode($tmhOAuth->response['response'], true);
				return $response['result']['places'][0]['full_name'];
			}
		}
		return false;
	}

}
